#!/usr/bin/env python3
'''
Reading model output data
..author: Coco Wu <jiewu@mhsinc.net>
'''

import s3fs
import pyarrow.parquet as pq
import ipdb
import pickle
import boto3

S3_BUCKET_NAME = 'mhspredict-site-fedex-louisville'

fs = s3fs.S3FileSystem()
client = boto3.client('s3')
resource = boto3.resource('s3')
my_bucket = resource.Bucket(S3_BUCKET_NAME)


prefix_pred = 'analytics_output/timeseries_predictions'
prefix_actual = 'fedex-louisville/sensor'





# looping through prediction folder and get the datefolder name, query the same date for the actual
# --> takes too long to read the acutal data (WITHDRAW this plan )
# for folder in fs.ls("s3://{bucket}/{pre}/".format(bucket=S3_BUCKET_NAME,pre=prefix_pred)):
# 	query_full_pred = "s3://{folder}".format(folder = folder)
# 	pred_output = pq.ParquetDataset(query_full_pred, filesystem=fs).read_pandas().to_pandas()
# 	datefolder = query_full_pred.split('/')[-1]
# 	query_full_actual = "s3://{bucket}/{pre}/{date}".format(bucket=S3_BUCKET_NAME,pre=prefix_actual,date=datefolder)
# 	actual_output = pq.ParquetDataset(query_full_actual, filesystem=fs).read_pandas().to_pandas()
# 	ipdb.set_trace()

# for folder in fs.ls("s3://{bucket}/{pre}/".format(bucket=S3_BUCKET_NAME,pre=predix_actual)):

bucket = resource.Bucket(name=S3_BUCKET_NAME)
FilesNotFound = True
for obj in bucket.objects.filter(Prefix=prefix):
	query_full_path2 = "s3://{bucket}/{obj}".format(bucket=S3_BUCKET_NAME, obj= obj.key)
	test1 = pq.ParquetDataset(query_full_path2, filesystem=fs).read_pandas().to_pandas()
	ipdb.set_trace()

ipdb.set_trace()

files = list(my_bucket.objects.filter(Prefix='analytics_output/timeseries_prediction/date=2018-11-29'))

ipdb.set_trace()
query_full_path2 = "s3://{bucket}/analytics_output/timeseries_predictions/date=2018-11-29".format(
	bucket=bucket_name)
fs = s3fs.S3FileSystem()
test1 = pq.ParquetDataset(query_full_path2, filesystem=fs).read_pandas().to_pandas()
