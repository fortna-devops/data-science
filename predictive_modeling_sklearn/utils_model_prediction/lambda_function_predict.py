#!/usr/bin/env python3
"""
Lambda Prediction for metadata extraction and ml predicting
..author: Coco Wu <jiewu@mhsinc.net>
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
import json
import pickle
from datetime import datetime
import pandas as pd
import s3fs

import numpy as np
import scipy
import scipy.stats
import pandas as pd
import boto3


from analytics_toolbox.data_init import data_retrieval, data_handles, data_writing
from analytics_sdk_core.constants import table_tags

import ipdb

TZOFFSET = 0
#
#
# DATA_RANGE = "2hour"
# DATA_RANGE = str(os.environ['data_range'])


def lambda_handler(event, context):
    bucket = event["bucket"]
    # initialize dqe for pulling data
    dqe = data_retrieval.DataQueryEngine(s3_bucket_name=bucket)
    # TODO: read in time interval (probably 2 hours)
    t1 = datetime(day=1, month=10, year=2019, hour=18)
    t2 = datetime(day=2, month=10, year=2019, hour=2)
    tt = (t1, t2)
    # tt = data_retrieval.construct_relative_timestamp_tuple(time_back=DATA_RANGE,tzoffset=TZOFFSET)

    conveyor_str = event["conveyor"].replace("-", "_")

    # directory holding all the models for a particular conveyor
    toplevel_dir = "s3://{bname}/models/timeseries_predictions/{c}".format(
        c=conveyor_str, bname=bucket)
    fs = s3fs.S3FileSystem()
    model_paths = fs.ls(toplevel_dir)
    # build up the union of all the input tags needed for each model
    master_taglist = set()
    # save models and metadata for later as list of tuple
    models_and_metadatas = {}
    # equipment types and sensor numbers
    sensors_and_equipment = set()

    # loop through all available models for conveyor
    for model_full_path in model_paths:

        with fs.open(model_full_path, 'rb') as fl:
            data = fl.read()

        # load model and metadata
        model_and_metadata = pickle.loads(data)
        model = pickle.loads(model_and_metadata['model'])
        metadata = model_and_metadata['metadata']

        et = metadata['equipment_type']

        if et in models_and_metadatas.keys():
            models_and_metadatas[et].append((model, metadata))
        elif et not in models_and_metadatas.keys():
            models_and_metadatas[et] = [(model, metadata)]

        # determine input tags for the model
        tags2query = metadata['query_tags']
        # only use tags that actually exist in the database (assumes constants is up to date)
        all_avail_tags = table_tags.TAG_LOCATIONS.keys()
        tags2query = set(tags2query) & set(all_avail_tags)

        # get all of the tags upfront to create one big dataframe to prevent multiple queries
        master_taglist = master_taglist | tags2query

    # query the data needed for all models once
    temp_data_dict = dqe.execute_timeseries_data_query(
        taglist=master_taglist,
        conveyor=event["conveyor"],
        equip="all",
        timerange_tuple=tt,
        return_as_df=True)
    aggregated_df = data_handles.merge_disparate_dfs(
        list(temp_data_dict.values()), merge_method='nearest', merge_tol='15s')
    aggregated_df.dropna(inplace=True)

    all_predictions = pd.DataFrame()
    for eq_type, mm_list in models_and_metadatas.items():
        # if eq_type == 'STRUCT':
        #     pass
        # else:
        #     continue
        equipment_predictions = pd.DataFrame()
        for mm in mm_list:

            model = mm[0]
            metadata = mm[1]

            model_input_df = pd.DataFrame()

            for c in metadata['model_input_datarange'].keys():
                model_input_df[c] = aggregated_df[c]
            # make predictions
            preds = model.predict(model_input_df)
            target_colname = metadata['model_target']
            target_colname_actual = metadata['model_target']
            try:
                target_colname = target_colname.split('.')[1]
            except IndexError:
                target_colname = target_colname
            preds = pd.DataFrame({target_colname: preds})
            preds.index = model_input_df.index

            equipment_predictions = pd.concat([equipment_predictions, preds], axis=1)
            equipment_predictions['{}_residuals'.format(target_colname)] = equipment_predictions[
                target_colname] - aggregated_df[target_colname_actual]

        equipment_predictions = equipment_predictions.resample('H').mean()

        # uses the last metadata bc they are all the same sensor number
        equipment_predictions['sensor'] = metadata["sensor_num"]
        equipment_predictions['conveyor'] = event["conveyor"]
        equipment_predictions['equipment_type'] = eq_type
        equipment_predictions['timestamp'] = pd.to_datetime(equipment_predictions.index)

        # add all rows

        all_predictions = all_predictions.append(equipment_predictions)
        # all_predictions_hour = all_predictions.resample('H').mean()
    dw = data_writing.DataWriter(s3_bucket_name=bucket)
    # dw.write_parquet_ts_predictions_data(all_predictions)
    dw.write_parquet_ts_predictions_data(all_predictions, table_name='timeseries_predictions_hour')


lambda_handler({"bucket": "mhspredict-site-dhl-miami",
                "conveyor": "LF-F1-1"},
               {})
