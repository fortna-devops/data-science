#!/usr/bin/env python3


from datetime import date, timedelta
import numpy as np
import scipy
import scipy.stats as st
import pickle
import pandas as pd
from analytics_toolbox.data_init import data_retrieval, data_handles


def comp_params(data, distribution):
    dist = getattr(scipy.stats, distribution)
    param = dist.fit(data)
    return (param[0], param[1])


def comp_residual_upper_limit(params, distribution, sensitive_level):
    if distribution == 'norm':
        residual_upper_level = np.round(scipy.stats.norm(params[0], params[1]).ppf(sensitive_level), 3)
        residual_lower_level = np.round(scipy.stats.norm(params[0], params[1]).ppf(1 - sensitive_level), 3)
    elif distribution == 't':
        residual_upper_level = np.round(scipy.stats.t(params[0], params[1]).ppf(sensitive_level), 3)
        residual_lower_level = np.round(scipy.stats.t(params[0], params[1]).ppf(1 - sensitive_level), 3)
    elif distribution == 'cauchy':
        residual_upper_level = np.round(scipy.stats.cauchy(params[0], params[1]).ppf(sensitive_level), 3)
        residual_lower_level = np.round(scipy.stats.cauchy(params[0], params[1]).ppf(1 - sensitive_level), 3)
    elif distribution == 'chi':
        residual_upper_level = np.round(scipy.stats.chi(params[0], params[1]).ppf(sensitive_level), 3)
        residual_lower_level = np.round(scipy.stats.chi(params[0], params[1]).ppf(1 - sensitive_level), 3)
    else:
        print("add some more distributions plz :)")
    return (residual_lower_level, residual_upper_level)


def make_pdf(dist, params, size=10000):
    """
    Generate distributions's Probability Distribution Function 
    """
    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]
    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)
    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)

    return pdf


def best_fit_distribution(data, bins=200, ax=None):
    """
    fit every distribution to residual and calculate sse to find out the best fit distribution
    """
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0
    # DISTRIBUTIONS = [
    #     st.alpha,  st.arcsine, st.beta, st.betaprime, st.bradford, st.burr, st.cauchy, st.chi, st.chi2, st.cosine,
    #     st.dgamma, st.dweibull, st.erlang, st.expon, st.exponnorm, st.exponweib, st.exponpow, st.f, st.fatiguelife, st.fisk,
    #     st.foldcauchy, st.foldnorm, st.frechet_r, st.frechet_l, st.genlogistic, st.genpareto, st.gennorm, st.genexpon,
    #     st.genextreme, st.gausshyper, st.gamma, st.gengamma, st.genhalflogistic, st.gilbrat, st.gompertz, st.gumbel_r,
    #     st.gumbel_l, st.halfcauchy, st.halflogistic, st.halfnorm, st.halfgennorm, st.hypsecant, st.invgamma, st.invgauss,
    #     st.invweibull, st.johnsonsb, st.johnsonsu, st.ksone, st.kstwobign, st.laplace, st.levy, st.levy_l, st.levy_stable,
    #     st.logistic, st.loggamma, st.loglaplace, st.lognorm, st.lomax, st.maxwell, st.mielke, st.nakagami, st.ncx2, st.ncf,
    #     st.nct, st.norm, st.pareto, st.pearson3, st.powerlaw, st.powerlognorm, st.powernorm, st.rdist, st.reciprocal,
    #     st.rayleigh, st.rice, st.semicircular, st.t, st.triang, st.truncexpon, st.truncnorm, st.tukeylambda,
    #     st.uniform, st.vonmises, st.vonmises_line, st.wald, st.weibull_min, st.weibull_max, st.wrapcauchy
    # ]
    DISTRIBUTIONS = [
        st.alpha, st.beta, st.cauchy, st.chi, st.chi2, st.cosine, st.erlang, st.expon, st.exponnorm,
        st.gamma, st.logistic, st.lognorm, st.lomax, st.maxwell, st.norm, st.pareto, st.pearson3,
        st.t, st.uniform
    ]
    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:
        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            # fit dist to data
            params = distribution.fit(data)
            # Separate parts of parameters
            arg = params[:-2]
            loc = params[-2]
            scale = params[-1]
            # Calculate fitted PDF and error with fit in distribution
            pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
            sse = np.sum(np.power(y - pdf, 2.0))
            # identify if this distribution is better
            if best_sse > sse > 0:
                best_distribution = distribution
                best_params = params
                best_sse = sse
        except Exception:
            pass

    return (best_distribution.name, best_params, best_sse)


def combine_residuals(data):
    return (data[0], data[2], data[3], data[1])

############ User Defined Variables - Start ##################
S3BUCKET_NAME = 'mhspredict-site-amazon-sdf4'
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

datestr1 = (date.today() - timedelta(days=7)).strftime('%Y-%m-%d')
datestr2 = (date.today()).strftime('%Y-%m-%d')
############ User Defined Variables  - End ##################

residual_file_name = f"{S3BUCKET_NAME.split('mhspredict-site-')[1]}_residual_threshold"
q_residuals = "SELECT sensor, rms_velocity_x_residuals,rms_velocity_z_residuals,temperature_residuals FROM timeseries_predictions WHERE date between date '{d1}' AND date '{d2}'".format(
    d1=datestr1, d2=datestr2)
res_residuals = dqe.execute_generic_query(q_residuals, return_as_df=True)
res_residuals.dropna(axis=0,  how='any', inplace=True)

res_residuals = res_residuals[np.abs(res_residuals.rms_velocity_x_residuals -
                                     res_residuals.rms_velocity_x_residuals.mean()) <= (3 * res_residuals.rms_velocity_x_residuals.std())]
res_residuals = res_residuals[np.abs(res_residuals.rms_velocity_z_residuals -
                                     res_residuals.rms_velocity_z_residuals.mean()) <= (3 * res_residuals.rms_velocity_z_residuals.std())]
res_residuals = res_residuals[np.abs(res_residuals.temperature_residuals -
                                     res_residuals.temperature_residuals.mean()) <= (3 * res_residuals.temperature_residuals.std())]
# Residual Research
tags = ['rms_velocity_x_residuals', 'rms_velocity_z_residuals', 'temperature_residuals']
params_dict = {}

# # Load data from statsmodels datasets
# adata = res_residuals['rms_velocity_z_residuals']
# Find best fit distribution
# best_fit_name, best_fit_params, sse = best_fit_distribution(adata, 200, ax=None)
# best_dist = getattr(st, best_fit_name)
# Make PDF with best params
# pdf = make_pdf(best_dist, best_fit_params)
# param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
# param_str = ', '.join(['{}={:0.2f}'.format(k, v) for k, v in zip(param_names, best_fit_params)])
# dist_str = '{}({})'.format(best_fit_name, param_str)

for tag in tags:
    params = comp_params(res_residuals[tag], 'norm')
    if 'temperature' in tag:
        yellow_threshold = comp_residual_upper_limit(params, 'norm', 0.90)
        red_threshod = comp_residual_upper_limit(params, 'norm', 0.90)
    else:
        yellow_threshold = comp_residual_upper_limit(params, 'norm', 0.90)
        red_threshod = comp_residual_upper_limit(params, 'norm', 0.90)
    residuals_thresholds = [red_threshod[0], yellow_threshold[0], yellow_threshold[1], red_threshod[1]]
    params_dict[tag] = residuals_thresholds

# with open('dhl_residual_threshold.pickle', 'wb') as handle:
#     pickle.dump(params_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

# with open('fedex_residual_threshold.pickle', 'rb') as handle:
#     test = pickle.load(handle)

with open(f'{residual_file_name}.pickle', 'wb') as handle:
    pickle.dump(params_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open(f'{residual_file_name}.pickle', 'rb') as handle:
    test1 = pickle.load(handle)
import ipdb
ipdb.set_trace()

# # Mean Absolute Error
# MAE = np.absolute(res_residuals[tag]).mean()
# # RMSE
# RMSE = np.sqrt((res_residuals[tag]**2).mean())
# print('MAE:{A},RMSE:{R} for tag:{T}'.format(A=MAE, R=RMSE, T=tag))

#    grouped_sensor['tag'] = tag + '_params'
#    grouped_sensor = res_residuals[['sensor', tag]].groupby(
#        'sensor').apply(comp_params, distribution='norm').reset_index(name='params')
#    ipdb.set_trace()
#    grouped_sensor['residual_level_0.80'] = grouped_sensor['params'].apply(
#        comp_residual_upper_limit, sensitive_level=0.80)
#    grouped_sensor['residual_level_0.70'] = grouped_sensor['params'].apply(
#        comp_residual_upper_limit, sensitive_level=0.70)
#    ipdb.set_trace()
#    grouped_sensor['residual_level'] = grouped_sensor.apply(
#        col1=grouped_sensor['residual_level_0.80'], col2=grouped_sensor['residual_level_0.70'])

# grouped_sensor = res_residuals[['sensor', tag]].groupby(
#     'sensor').apply(comp_params, distribution='norm').reset_index(name='params')
# grouped_sensor['tag'] = tag + '_params'
# grouped_sensor['residual_level_0.80'] = grouped_sensor['params'].apply(
#     comp_residual_upper_limit, sensitive_level=0.80)
# grouped_sensor['residual_level_0.70'] = grouped_sensor['params'].apply(
#     comp_residual_upper_limit, sensitive_level=0.70)
# grouped_sensor['residuals'] = grouped_sensor['residual_level_0.80'] + grouped_sensor['residual_level_0.70']
# grouped_sensor['residuals_thresholds'] = grouped_sensor['residuals'].apply(combine_residuals)
