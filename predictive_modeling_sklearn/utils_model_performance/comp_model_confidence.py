from utils_comp_model_confidence import *
from analytics_toolbox.data_init import data_retrieval

import pickle
import s3fs

S3_BUCKET_NAME = 'mhspredict-site-amazon-zappos'
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3_BUCKET_NAME)
conveyors = dqe.get_conveyors(S3_BUCKET_NAME)

tag_list = ['rms_velocity_x', 'rms_velocity_z', 'temperature']
conveyor_medadatatag = {}
for conveyor in conveyors:
    conveyor_dict = grab_metadata(S3_BUCKET_NAME, conveyor, 'mse')
    conveyor_flat_dict = flat_conveyor_dict(conveyor_dict, tag_list)
    tag_params_dict = generate_params_dict(conveyor_flat_dict)
    model_confindence_dict = compute_model_confidence(conveyor_dict, tag_params_dict)
    # if there is only one equipment in the conveyor, the confidence score
    # will be nan since there is only one value in the mse list. thus filling
    # 1 instead.
    for key, value in model_confindence_dict.items():
        if math.isnan(value):
            model_confindence_dict[key] = 1.0
    conveyor_medadatatag[conveyor] = model_confindence_dict

gateway_name = S3_BUCKET_NAME.split('mhspredict-site-')[1]
with open(f'{gateway_name}_confidence.pickle', 'wb') as handle:
    pickle.dump(conveyor_medadatatag, handle, protocol=pickle.HIGHEST_PROTOCOL)

# with open('fedex_model_confidence.pickle', 'rb') as handle:
#     test = pickle.load(handle)
# ipdb.set_trace()

# with open('gbm_HC_F1_3PT_M_rms_velocity_z.pickle', 'rb') as handle:
#     test = pickle.load(handle)
# ipdb.set_trace()

with open(f'{gateway_name}_confidence.pickle', 'rb') as handle:
    confidence = pickle.load(handle)
import ipdb
ipdb.set_trace()

assert conveyor_medadatatag == confidence
