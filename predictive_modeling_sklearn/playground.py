# def get_location_table(dqe):
#     """Summary
#     query location table
#     Args:
#         dqe (TYPE): class object

#     Returns:
#         TYPE: location table
#     """
#     query = """
#       SELECT sensor,conveyor,equipment,location
#       FROM location
#       WHERE sensor != 'sensor'
#       """

#     location = dqe.execute_generic_query(query, return_as_df=True)

#     return location

# # individual check

# def conveyor_sensor_dict(query_location_results):
#     """
#     process data from raw location results to the format conveyor sensor dictionary

#     :param location_query_results: query result from location table query
#     :data type: a list of ordered dictictionary

#     :returns: a dictionary with conveyor as key and a dictionary of equipment:sensor num as value
#     :rtype: nested dictionary
#     """
#     conveyor_sensor_dict = {}
#     for location_query_res in query_location_results:
#         conveyor = location_query_res['conveyor']
#         sensor_num = location_query_res['sensor']
#         location = location_query_res['location']
#         if conveyor in conveyor_sensor_dict.keys():
#             conveyor_sensor_dict[conveyor][location] = sensor_num
#         else:
#             equipment_dict = {}
#             equipment_dict[location] = sensor_num
#             conveyor_sensor_dict[conveyor] = equipment_dict
#     return conveyor_sensor_dict

# sensor_conveyor_query = f"""
# SELECT conveyor, equipment
# FROM "{S3BUCKET_NAME}".location
# WHERE sensor = '{sensor}'
# AND date BETWEEN date '{datestrbefore}' AND date '{datestrnw}'
# AND timestamp BETWEEN timestamp '{before}' AND timestamp '{nw}'"""

# on_percentage = round(len(query_data_results) / int(total_num_res[0]['count']), 2)
# total_num_query = f"""
# SELECT count(*) as count
# FROM "{S3BUCKET_NAME}".sensor
# WHERE sensor = '{sensor}'
# AND date BETWEEN date '{datestrbefore}' AND date '{datestrnw}'
# AND timestamp BETWEEN timestamp '{before}' AND timestamp '{nw}'"""
# total_num_res = dqe.execute_generic_query(total_num_query)

# sensor_conveyor_res = dqe.execute_generic_query(sensor_conveyor_query)
# mpath = 'models/timeseries_predictions/PF_F1_5PT/gbm_PF_F1_5PT_M_temperature.pickle'
# model_full_path = "s3://{bname}/{mpath}".format(bname="mhspredict-site-dhl-miami", mpath=mpath)
# with fs.open(model_full_path, 'rb') as fl:
#     data = fl.read()
# model_and_metadata = pickle.loads(data)
# model = pickle.loads(model_and_metadata['model'])
# metadata = model_and_metadata['metadata']
# ipdb.set_trace()

# books = [(16, 2), (10, 5), (2, 2)]

# book_dict = {}
# for book in books:
#     book_dict['credit'] = book[0]
#     book_dict['weight'] = book[1]

# credit_score = []
# for credit, weight in books.items():
#     if credit not in credit_score:
#         credit_score.append(credit)

# # for one_book in book_dict:

# import pandas as pd
# import ipdb
# data1 = pd.read_excel('SIS/SIS2_Data_Report.xlsx', sheet_name='Data')
# data2 = pd.read_excel('SIS/SIS2_Data_Report2.xlsx', sheet_name='Data')
# data3 = pd.read_excel('SIS/SIS2_Data_Report3.xlsx', sheet_name='Data')
# result1 = data1.append(data2)
# result2 = result1.append(data3)
# result2.to_csv('SIS_all.csv', index=False)
# ipdb.set_trace()

# import random
# from datetime import datetime, timedelta
# import pandas as pd
# import ipdb

# min_year = 2018
# max_year = 2018

# start = datetime(min_year, 1, 1, 00, 00, 00)
# years = max_year - min_year + 1
# end = start + timedelta(days=365 * years)

# df = pd.DataFrame()
# datelist = []
# for i in range(140):
#     random_date = start + (end - start) * random.random()
#     timestamp_string = random_date.strftime('%Y-%m-%d')
#     datelist.append(timestamp_string)
# df['date'] = datelist
# ipdb.set_trace()


# def gen_datetime(min_year=min_year, max_year=max_year):
#     # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
#     start = datetime(min_year, 1, 1, 00, 00, 00)
#     years = max_year - min_year + 1
#     end = start + timedelta(days=365 * years)
#     return start + (end - start) * random.random()

# from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
# import json
# import ipdb

# buckets = ['mhspredict-site-dhl-miami', 'mhspredict-site-fedex-louisville']
# dhl = buckets[0]
# fedex = buckets[1]

# dqe = DQE(s3_bucket_name=dhl)
# import ipdb
# from analytics_toolbox.data_init import data_retrieval
# dqe = data_retrieval.DataQueryEngine(s3_bucket_name='mhspredict-site-dhl-miami')
# all_sensors_query = "SELECT * FROM sensor where date between date '2019-06-01' and date '2019-06-03' and timestamp between timestamp '2019-06-01 00:00:00' and timestamp '2019-06-03 12:00:00'"
# all_sensors = dqe.execute_generic_query(all_sensors_query, return_as_df=True)
# ipdb.set_trace()
# low_confidence_model = {}
# for location in all_locations:
#     model_confidence = json.loads(location['alarms_confidence'])
#     sensor_num = location['sensor']
#     conveyor_num = location['conveyor']
#     for tag, confidence_score in model_confidence.items():
#         if confidence_score < 0.001:
#             model = '{s}-{t}'.format(s=sensor_num, t=tag)
#             low_confidence_model[model] = (conveyor_num, confidence_score)
# ipdb.set_trace()

# from analytics_sdk_core.data_init import data_pull
# from analytics_sdk_core.testing_utilities import mock_data
# import ipdb

# s3_bucket_name = 'mhspredict-site-fedex-louisville'
# dqe = data_pull.DataQueryEngineLite(s3_bucket_name=s3_bucket_name)

# sensor_tuple = tuple(range(1, 21))
# sensor_tuple = tuple(map(str, sensor_tuple))
# location_table = pd.DataFrame(dqe.execute_generic_query(
#     'select * from location where sensor in {}'.format(sensor_tuple)))


# location_table = mock_data.create_mock_location_data(sensors=20)

import pandas as pd
import numpy as np
import ipdb
location_ini = pd.read_csv('location_table_format.csv')
for id, df_i in enumerate(np.array_split(location_ini, len(location_ini))):
    df_i.to_csv('location/location_{}.csv'.format(df_i['sensor'][id]), index=False, na_rep='NULL')


ipdb.set_trace()


# import boto3

# import s3fs
# from dateutil import parser

# from analytics_sdk_core.data_init import data_write_lite
# from analytics_sdk_core.analytic_exceptions import exceptions
# from analytics_sdk_core.constants import timezones
# from analytics_sdk_core.constants.card_colors import RED, YELLOW, GREEN
# from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
# from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DW
# import time
# import ast
# import ipdb
# import json
########## UPDATE LOCATIONS THRESHOLDS #######

# todo automatically download backup of s3 files recursively from location table before making updates

# buckets = 'mhspredict-site-mhs-emulated'

# gateway = 'mhs-emulated'

# dqe = DQE(s3_bucket_name=buckets)
# # dw = DW(s3_bucket_name=buckets)

# all_locations_query = "SELECT * FROM location"
# all_locations = dqe.execute_generic_query(all_locations_query)

# ipdb.set_trace()
# # ipdb.set_trace()
# # sensors2change = ['44', '47']
# # gateway_name = 'fedex-louisville'
# # for x in all_locations:
# #     if x['sensor'] in sensors2change:
# #         if x['alarms_confidence'] == '':
# #             sensor = x['sensor']
# #             NEWCOL = 'alarms_confidence'
# #             NEWVAL = '{}'
# #             dw._create_column_location_data(sensor, NEWCOL, NEWVAL, gateway_name)


# for x in all_locations:
#     if x['equipment'] == 'B':
#         thresholds = {"temperature": {"red": 250, "orange": 200}, "hf_rms_acceleration_x": {"red": 6.0, "orange": 1.5}, "hf_rms_acceleration_z": {
#             "red": 6.0, "orange": 1.5}, "rms_velocity_z": {"red": 0.37, "orange": 0.18}, "rms_velocity_x": {"red": 0.37, "orange": 0.18}}
#         dw.write_location_data(
#             sensor=str(x['sensor']),
#             col='thresholds',
#             value=thresholds,
#             gateway_name=gateway
#         )
# ipdb.set_trace()

# robotics data processing
