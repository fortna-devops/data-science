# data-science

Repository for data science work, including, but not limited to:
- model training
- model evaluation
- model predictions
- data preparation 
- data visualization

Some overlapping functionality/code with analytics-exploration repository