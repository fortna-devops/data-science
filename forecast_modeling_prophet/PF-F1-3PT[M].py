
# coding: utf-8

# In[251]:


from analytics_toolbox.data_init import data_retrieval
from datetime import datetime
import fbprophet
import pandas as pd
import matplotlib.pyplot as plt
pd.plotting.register_matplotlib_converters()


# ### Query Sep.10 - Oct.1 and Predict Oct.2

# In[181]:


S3BUCKET_NAME = 'mhspredict-site-dhl-miami'
tags = [
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x'
]
t1 = datetime(day=10, month=9, year=2019, hour=0)
t2 = datetime(day=1, month=10, year=2019, hour=23)
timetuple = (t1, t2)
conveyor = 'PF-F1-3PT'
equipment = 'M'


# In[33]:


dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)
temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=tags,
    timerange_tuple=timetuple,
    conveyor=conveyor,
    equip=equipment,
    return_as_df=True)


# In[34]:


pred_data = temp_data_dict.reset_index()[['timestamp', 'temperature']]


# In[35]:


pred_data = pred_data.rename(columns={'timestamp': 'ds', 'temperature': 'y'})


# In[36]:


pred_data['ds'] = pred_data['ds'].dt.strftime('%Y-%m-%d %H:%M:%S')


# In[37]:


pred_data


# In[38]:


model = fbprophet.Prophet()


# In[39]:


model.fit(pred_data)


# In[40]:


model_forecast = model.make_future_dataframe(periods= 24, freq='H')


# In[41]:


model_forecast = model.predict(model_forecast)


# In[42]:


fig = model.plot(model_forecast)


# In[43]:


fig = model.plot_components(model_forecast)


# In[159]:


model_forecast_Oct_2 = model_forecast.loc[(model_forecast['ds'] >= '2019-10-2')][['ds','yhat']]


# In[160]:


model_forecast_Oct_2.index = model_forecast_Oct_2['ds']


# In[161]:


model_forecast_Oct_2 = model_forecast_Oct_2.resample('H').mean()


# ## compare with actual data (Oct-02-2019)

# In[46]:


t1 = datetime(day=2, month=10, year=2019, hour=0)
t2 = datetime(day=2, month=10, year=2019, hour=23)
timetuple = (t1, t2)
conveyor = 'PF-F1-3PT'
equipment = 'M'


# In[49]:


temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=['temperature'],
    timerange_tuple=timetuple,
    conveyor=conveyor,
    equip=equipment,
    return_as_df=True)


# In[156]:


ori_data = temp_data_dict.sort_index().resample('H').mean()


# In[167]:


ori_data.index = pd.to_datetime(ori_data.index)


# In[178]:


model_forecast_Oct_2[['yhat']].plot()


# In[176]:


ori_data[['temperature']].plot()


# In[179]:


comb_data = ori_data.merge(model_forecast_Oct_2, left_index =True, right_index = True)


# In[180]:


comb_data[['temperature', 'yhat']].plot()


# ### Query Sep.20 - Oct.21 and Predict Oct.22 - Oct.26

# In[182]:


tags = [
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x'
]
t1 = datetime(day=20, month=9, year=2019, hour=0)
t2 = datetime(day=21, month=10, year=2019, hour=23)
timetuple = (t1, t2)
conveyor = 'PF-F1-3PT'
equipment = 'M'


# In[183]:


temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=tags,
    timerange_tuple=timetuple,
    conveyor=conveyor,
    equip=equipment,
    return_as_df=True)


# In[184]:


pred_data = temp_data_dict.reset_index()[['timestamp', 'rms_velocity_x']]
pred_data = pred_data.rename(columns={'timestamp': 'ds', 'rms_velocity_x': 'y'})
pred_data['ds'] = pred_data['ds'].dt.strftime('%Y-%m-%d %H:%M:%S')


# In[186]:


model = fbprophet.Prophet()
model.fit(pred_data)


# In[189]:


model_forecast = model.make_future_dataframe(periods= 24 * 5, freq='H')
model_forecast = model.predict(model_forecast)
fig = model.plot(model_forecast)


# In[191]:


fig = model.plot_components(model_forecast)


# In[223]:


model_forecast.head()
model_forecast_Oct_20 = model_forecast.loc[(model_forecast['ds'] >= '2019-10-20')]
model_forecast_Oct_20.index = model_forecast_Oct_20['ds']
model_forecast_Oct_20[['yhat_lower', 'yhat_upper', 'yhat']].plot()


# In[216]:


model_forecast_Oct_20


# In[225]:


model_forecast_Oct_22 = model_forecast.loc[(model_forecast['ds'] >= '2019-10-22')][['ds','yhat','yhat_lower','yhat_upper']]


# In[226]:


model_forecast_Oct_22.index = pd.to_datetime(model_forecast_Oct_22['ds'])


# In[227]:


model_forecast_Oct_22 = model_forecast_Oct_22.sort_index().resample('H').mean()


# In[192]:


# query actual Oct 22, 23rd data
t1 = datetime(day=22, month=10, year=2019, hour=0)
t2 = datetime(day=23, month=10, year=2019, hour=23)
timetuple = (t1, t2)
conveyor = 'PF-F1-3PT'
equipment = 'M'
temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=tags,
    timerange_tuple=timetuple,
    conveyor=conveyor,
    equip=equipment,
    return_as_df=True)


# In[193]:


ori_data = temp_data_dict.sort_index().resample('H').mean()


# In[198]:


ori_data.head()


# In[199]:


model_forecast_Oct_22.head()


# In[210]:


model_forecast_Oct_22


# In[228]:


comb_data = ori_data.merge(model_forecast_Oct_22, left_index =True, right_index = True, how = 'right')


# In[230]:


comb_data[['rms_velocity_x', 'yhat', 'yhat_upper', 'yhat_lower']].plot()


# In[213]:


comb_data


# ### Query Aug.20 - Oct.21 and Predict Oct.22 - Oct.26

# In[233]:


tag = 'rms_velocity_x'
t1 = datetime(day=22, month=8, year=2019, hour=0)
t2 = datetime(day=21, month=10, year=2019, hour=23)
timetuple = (t1, t2)
conveyor = 'PF-F1-3PT'
equipment = 'M'


# In[234]:


def forcast_learning(tag, conveyor, equipment, timetuple, sensitive_level):
    """
    :param tag: tag name
    :rtype: list
    
    :param timetuple: datatime tuple
    :rtype: tuple
    
    :param sensitive level: default: 0.05; higher value means more sensitive, lower means less sensitive
    :rtype: float
    """
    data = dqe.execute_timeseries_data_query(
        taglist=[tag],
        timerange_tuple=timetuple,
        conveyor=conveyor,
        equip=equipment,
        return_as_df=True)
    
    pred_data = data.reset_index()[['timestamp', tag]]
    # rename the column
    pred_data = pred_data.rename(columns={'timestamp': 'ds', tag: 'y'})
    pred_data['ds'] = pred_data['ds'].dt.strftime('%Y-%m-%d %H:%M:%S')
    
    model = fbprophet.Prophet(changepoint_prior_scale = sensitive_level)
    model.fit(pred_data)
    return model


# In[235]:


model = forcast_learning('rms_velocity_x', conveyor, equipment, timetuple, sensitive_level = 0.15)


# In[255]:


# changepoints represent when the timeseries grouowth rate significantly changes
model_changepoint = [str(date) for date in model.changepoints]
model_changepoint


# In[239]:


model_forecast = model.make_future_dataframe(periods= 24 * 5, freq='H')
model_forecast = model.predict(model_forecast)
fig = model.plot(model_forecast)


# In[244]:


ax.vlines(model_changepoint, ymin = 0, ymax= 4, colors = 'r', linewidth=0.6, linestyles = 'dashed', label = 'Changepoints')


# In[243]:


ax = fig.add_subplot(1, 1, 1)


# In[254]:


ax.figure


# In[260]:


model_forecast_Oct22 = model_forecast.loc[(model_forecast['ds'] >= '2019-10-22')][['ds','yhat_lower', 'yhat_upper','yhat']]


# In[261]:


model_forecast_Oct22.index = model_forecast_Oct22['ds']


# In[264]:


model_forecast_Oct22.resample('H').mean().plot()

