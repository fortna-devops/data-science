#!/usr/bin/env python3
'''
optimal spare part calculation module
..author: Coco Wu <jiewu@mhsinc.net>
'''

import numpy as np
import scipy.stats as st
import math

from pint import UnitRegistry
ureg = UnitRegistry()

"""
# Mean Time Between Failure
# Failure Rate : https://en.wikipedia.org/wiki/Failure_rate
# Failure_Rate = 1/mean_time_between_failure
------------------------------
    User Input Values - start
------------------------------
"""
mean_time_between_failure = 50000000  # (unit in seconds)
# if enters a string " XX years" it will returns stock value for year 1, year 2, year 3, ...year 7
# elif enter a float 120 it will returns stock value for 120 seconds observation window
operating_time_of_interest = '5 years'
num_units = 500
confidence_level = 0.90
simulation_iteration = 10000
"""
-------------------------------
    User Input Values - End
-------------------------------
"""

"""
# expected_failure_nums = N * Failure_Rate * Within_Time
# safety_stock =  (Maximum daily usage * Maximum lead time in days) – (Average daily usage * Average lead time in days)


------------------------
Exponential Distribution
------------------------
The exponential distribution is one of the widely used continuous distributions. 
It is often used to model the time elapsed between events. 

---------------------
Poisson Distribution 
---------------------
given an equipment population (n), the failure rate of each equipment (λ) 
and a time interval (t) during which no further spares will be available, 
the Poisson distribution can be used to predict the number of failures that will occur, 
 with a certain probability (or confidence), and thus the number of depot spares required for time interval t.
"""


def generate_rv_exponential_distribution(mean_time_between_failure):
    """
    generate ramdom variable from exponential distribution

    :param mean_time_between_failure: mean time between failure
    :mean_time_between_failure type: float

    :returns: time between failure generate from exponential distribution
    :rtype: float
    """
    U = np.random.uniform()
    time_between_failure = math.log(1 - np.random.uniform()) / (-1 / mean_time_between_failure)
    return time_between_failure


def probability_stock_level(average_number_event, n):
    """
    compute the the probability of failure units is smaller than the stock level based on the confidence level

    :param average_number_event: expected number of failure units
    :average_number_event type: int

    :param n: number of united supported
    :n type: int

    :returns: probability of safe stock
    :rtype: float, between 0 and 1 
    """
    return np.exp(-average_number_event) * (average_number_event)**n / math.factorial(n)


def compute_optimal_stock_level(mean_time_between_failure, time_interval, num_units, confidence_level):
    """
    compute optimal stock level based on mean time between failure (hours, time interval (hours)
    number of units to be supported and desird confidence

    verify result here: https://reliabilityanalyticstoolkit.appspot.com/poisson_spares_analysis


    :param mean_time_between_failure: mean time between failure
    :mean_time_between_failure type: float, unit in hours

    :param time_interval: time interval for observe
    :time_interval type: int, unit in hours 

    :param num_units: number of units used on site 
    :num_units type: int

    :param confidence_level: probability of having few items failure
    :confidence_level: float, number between 0 and 1

    """
    z_score = st.norm.ppf(confidence_level)
    stock = 0
    # simulate TBF based on the expoential distribution
    average_number_event = 1 / mean_time_between_failure * num_units * time_interval

    if average_number_event < 720:
        try:
            probability_initial_stock = probability_stock_level(average_number_event, stock)
            while probability_initial_stock < confidence_level:
                stock = stock + 1
                probability_initial_stock = probability_initial_stock + \
                    probability_stock_level(average_number_event, stock)
        except OverflowError:
            pass
    else:
        stock = ((z_score + np.sqrt(z_score**2 + 4 * (average_number_event))) ** 2) / 4
    return math.ceil(stock)


def run_simulation(num_iterations, mean_time_between_failure, time_interval, num_units, confidence_level):
    """
    run simulation
    """
    optimal_stock_list = []
    for i in range(num_iterations):
        TBF = generate_rv_exponential_distribution(mean_time_between_failure)
        average_number_event = 1 / TBF * num_units * time_interval
        optimal_stock = compute_optimal_stock_level(TBF, time_interval, num_units, confidence_level)
        optimal_stock_list.append(optimal_stock)

    return optimal_stock_list


def optimal_stock_level_after_simulation(optimal_stock_list):
    """
    compute the average of 5 percentile to 95 percentile for the optimal stock level
    """

    len_list = len(optimal_stock_list)
    percentile_5 = math.floor(len_list * 0.05)
    percentile_95 = math.ceil(len_list * 0.95)
    non_negative_list = [i for i in optimal_stock_list if i >= 0]
    sorted_list = sorted(non_negative_list)
    filtered_list = sorted_list[percentile_5:percentile_95]
    optimal_stock = math.ceil(sum(filtered_list) / len(filtered_list))

    return optimal_stock


confidence_level_map = {"low_risk": 0.95, "medium_risk": 0.90, "high_risk": 0.85}
# recognize type of operatiing_time_of_interest and calculate final optmizal stock value
if not isinstance(operating_time_of_interest, str):
    optimal_stock_list = run_simulation(simulation_iteration, mean_time_between_failure,
                                        operating_time_of_interest, num_units, confidence_level)
    optimal_stock = optimal_stock_level_after_simulation(optimal_stock_list)

elif isinstance(operating_time_of_interest, str):
    quant_int = ureg(operating_time_of_interest).magnitude
    quant_unit = ureg(operating_time_of_interest).units
    optimal_stock = {}
    for quant in range(quant_int):
        quant_next = quant + 1
        quant_current_str = ureg(f"{quant} {quant_unit}")
        quant_next_str = ureg(f"{quant_next} {quant_unit}")
        quant_current_sec = quant_current_str.to('seconds').magnitude
        quant_next_sec = quant_next_str.to('seconds').magnitude
        optimal_stock_list_current = run_simulation(simulation_iteration, mean_time_between_failure,
                                                    quant_current_sec, num_units, confidence_level)
        optimal_stock_current = optimal_stock_level_after_simulation(optimal_stock_list_current)
        print(f'the optimal stock units needed for {quant} {quant_unit} is {optimal_stock_current}')
        optimal_stock_list_next = run_simulation(simulation_iteration, mean_time_between_failure,
                                                 quant_next_sec, num_units, confidence_level)
        optimal_stock_next = optimal_stock_level_after_simulation(optimal_stock_list_next)
        print(f'the optimal stock units needed for {quant + 1} {quant_unit} is {optimal_stock_next}')
        optimal_stock_level = optimal_stock_next - optimal_stock_current
        optimal_stock[f"{quant_next} {quant_unit}"] = optimal_stock_level

print(optimal_stock)
